use demo;

create table worker(
worker_id int Primary Key,
first_name varchar(25) not Null,
last_name varchar(25),
salary int check (salary between 10000 and 25000),
joining_date datetime,
department char(25) check (department in('HR','Sales','Accts','IT'))
);

insert into worker values
(9,'karan               ','singh',12000,'2023-05-01 12:40:12','hr');,
(2,'aman','singh',10000,'2023-05-01 12:40:12','it'),
(3,'ram','singh',14000,'2023-05-01 12:40:12','sales'),
(4,'rahul','singh',12000,'2023-05-01 12:40:12','hr'),
(5,'naman','singh',16000,'2023-05-01 12:40:12','sales'),
(6,'om','singh',22000,'2023-05-01 12:40:12','accts'),
(7,'jai','singh',20000,'2023-05-01 12:40:12','accts'),
(8,'hari','singh',24000,'2023-05-01 12:40:12','it');

select * from worker;

select first_name as WORKER_NAME from worker;

select Upper(first_name) from worker; 

select DISTINCT(department) from worker;

select LEFT(first_name,3) from worker;

SELECT CHARINDEX('a', 'karan');

select RTRIM(first_name) from worker;

select LTRIM(department) from worker;

select DISTINCT(department), LEN(department) as DeptLength from worker;

select REPLACE(first_name,'a','A') from worker;

select CONCAT(first_name,' ',last_name) as full_name from worker;

select * from worker order by first_name;

select * from worker order by first_name ASC,department DESC;

select * from worker where first_name  in ('karan','aman');

select * from worker where first_name not in ('karan','aman');

select * from worker where department like 'hr';

select COUNT(worker_id) as workr_in_accts from worker where department like 'accts';

select * from worker where salary between 10000 and 25000;

select * from worker where RIGHT(first_name,1) ='n';

select * from worker where CHARINDEX('a',first_name) > 0;

select * from worker where RIGHT(first_name,1) = 'h' and LEN(first_name)=6;

select * from worker where MONTH(joining_date) = 02 and YEAR(joining_date)=2014;

insert into worker values(10,'shriah','sah',13000,'2014-02-01 12:40:12','it');